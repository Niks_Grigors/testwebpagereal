<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\URL;

class testWebPageController extends Controller
{
    function index(){
        return view('pages.homepage');
    }

    function signup(){
        return view('pages.signup');
    }

    function services(){
        return view('pages.services');
    }

    function about(){
        return view('pages.about');
    }
    function contact(){
        return view('pages.contact');
    }
    
    function faq(){
       return view('pages.faq');
    }

    function register(Request $req){
        $result=DB::insert("insert ignore into posts(name,email) values(?,?)",[$req->input('name'),$req->input('email')]);

        return view('pages.register');
    }
}
