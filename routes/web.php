<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//Route::get('/',[App\Http\Controllers\testWebPageController::class, 'index']);


Route::get('/',[App\Http\Controllers\testWebPageController::class, 'index']);
Route::get('/about',[App\Http\Controllers\testWebPageController::class, 'about']);
Route::get('/services',[App\Http\Controllers\testWebPageController::class, 'services']);
Route::get('/contact',[App\Http\Controllers\testWebPageController::class, 'contact']);
Route::get('/faq',[App\Http\Controllers\testWebPageController::class, 'faq']);
Route::get('/signup',[App\Http\Controllers\testWebPageController::class, 'signup']);
Route::post('/register',[App\Http\Controllers\testWebPageController::class, 'register']);
