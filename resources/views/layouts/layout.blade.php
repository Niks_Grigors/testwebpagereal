<!DOCTYPE html>
<html lang="en">
<head>
  <title>testWebSite</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link type="text/css" href="css/style.css" rel="stylesheet">
</head>
<body>

<header class="header">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">                        
      </button>
      <a class="navbar-brand" href="{{('/')}}"><img src="/images/unnamed.png" alt="Logo" width="100px;"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class ="nav-item active"><a href="{{('/')}}">HOME</a></li>
        <li class ="nav-item active"><a href="{{('/services')}}">SERVICES</a></li>
        <li class ="nav-item active"><a href="{{('/about')}}">ABOUT</a></li>
        <li class ="nav-item active"><a href="{{('/contact')}}">CONTACT</a></li>
        <li class ="nav-item active"><a href="{{('/faq')}}">FAQ</a></li>
        <li><button type="button" class="navbar-btn btn btn-custom"><a href="{{('/signup')}}">SIGN UP</a></button></li>
        </div>
      </ul>
    </div>
  </div>
</nav>
</header>
<div class="row">
    <div class="col-sm-4">
    @yield('content')    
    </div>
<div class="col-sm-8">
    <div class="content">
      <img src="/images/illustration.jpg" alt="illustration logo" style="float: right;"/>
      
  
    </div>
  </div>  

</body>
</html>