<!DOCTYPE html>
<html lang="en">
<head>
  <title>testWebSite</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link type="text/css" href="css/style.css" rel="stylesheet">
</head>
<body>
	
    <div class="container">
		<div class="row_to_move">
			<div class="panel panel-primary">
				<div class="panel-body">
					<form method="POST" action="{{URL::to('/register')}}" role="form">
						@csrf
						<div class="form-group">
							<h2 class="formh2">Sign Up</h2>
						</div>
						<div class="form-group">
							<label class="control-label" for="signupName">Your name</label>
							<input name="name" id="signupName" required="required" type="text" maxlength="50" class="form-control">
						</div>	
						<div class="form-group">
							<label class="control-label" for="signupEmail">Email</label>
							<input name="email" id="signupEmail" required="required" type="email" maxlength="50" class="form-control">
						</div>
					
						<div class="form-group">
							<button  id="signupSubmit" type="submit" class="btn btn-info btn-block">Sign Up</button>
						</div>
						<p class="form-group">By creating an account, you agree to our <a href="#">Terms of Use</a> and our <a href="#">Privacy Policy</a>.</p>
						<a class="hyperLink" href="{{('/')}}" style="text-align:center">Go back to the Homepage</a>
						<hr>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>


